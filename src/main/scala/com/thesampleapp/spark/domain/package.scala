package com.thesampleapp.spark

import java.sql.Date

package object domain {

  case class AccessEntry(ipAddress: String,
                         date: Date,
                         httpMethod: String,
                         httpPath: String,
                         httpResponseCode: Int)
}
