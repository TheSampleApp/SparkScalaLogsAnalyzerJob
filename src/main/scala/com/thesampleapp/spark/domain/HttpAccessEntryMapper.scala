package com.thesampleapp.spark.domain

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.regex.Pattern

trait HttpAccessEntryMapper {
  def map(logEntryLine: String): Option[AccessEntry]
}

class HttpAccessEntryMapperImpl extends HttpAccessEntryMapper{
  val regex = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"(.+?)\""
  val pattern = Pattern.compile(regex)
  val df = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z", Locale.US)

  override def map(logEntryLine: String): Option[AccessEntry] = {

    val matcher = pattern.matcher(logEntryLine)
    matcher.find() match {
      case true =>
        val date = df.parse(matcher.group(4))
        val httpCommand = matcher.group(5)
        val httpCommandFields = httpCommand.split(" ")
        val httpMethod = httpCommandFields(0)
        val httpPath = httpCommandFields(1)
        val httpResponse = matcher.group(6).toInt
        Some(AccessEntry(ipAddress = matcher.group(1),
          date = new Date(date.getTime),
          httpMethod = httpMethod,
          httpPath = httpPath,
          httpResponseCode = httpResponse))
      case false => None
    }
  }
}

object HttpAccessEntryMapper{
  def apply(): HttpAccessEntryMapper = new HttpAccessEntryMapperImpl()
}