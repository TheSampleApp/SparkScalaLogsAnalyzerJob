package com.thesampleapp.spark

import com.thesampleapp.spark.domain.HttpAccessEntryMapper
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql._

object SparkScalaLogsAnalyzerJob extends App{

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession
    .builder
    .appName("SparkScalaLogsAnalyzerJob")
    .master("local[*]")
    .getOrCreate()

  val lines = spark.sparkContext.textFile("data/access_log")
  val httpAccessEntryMapper = HttpAccessEntryMapper()
  val accessEntriesRdd = lines.flatMap(httpAccessEntryMapper.map(_))
  import spark.implicits._
  val accessEntriesDs = accessEntriesRdd.toDS()
  val ipAddressCountDs = accessEntriesDs.groupBy("ipAddress").count()
  ipAddressCountDs.write.csv("out/ipcount.csv")
  spark.stop()
}
