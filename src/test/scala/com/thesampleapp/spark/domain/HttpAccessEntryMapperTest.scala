package com.thesampleapp.spark.domain

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class HttpAccessEntryMapperTest extends AnyFlatSpec with should.Matchers {
  "HttpAccessEntryMapper" should "map the HttpAccessEntry" in {
    val httpAccessEntryMapper = HttpAccessEntryMapper()
    val logEntry = "123.123.123.123 - - [27/Oct/2000:09:27:09 -0400] \"GET /java/javaResources.html " + "HTTP/1.0\" 200 10450 \"-\" \"Mozilla/4.6 [en] (X11; U; OpenBSD 2.8 i386; Nav)\""
    val res = httpAccessEntryMapper.map(logEntryLine = logEntry)
    res.isDefined should be (true)
    val httpAccessEntry = res.get
    httpAccessEntry.ipAddress should be("123.123.123.123")
    httpAccessEntry.httpPath should be("/java/javaResources.html")
    httpAccessEntry.httpMethod should be("GET")
    httpAccessEntry.date != null should be(true)
    httpAccessEntry.httpResponseCode should be(200)
  }

  "HttpAccessEntryMapper" should "map the HttpAccessEntry2" in {
    val httpAccessEntryMapper = HttpAccessEntryMapper()
    val logEntry = "123.123.123.123 - - [27/Mar/2023:16:31:48 +0000] \"GET /images/gradient.svg HTTP/1.1\" 200 308 \"https://thesampleapp.site/css/styles.css\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36\""
    val res = httpAccessEntryMapper.map(logEntryLine = logEntry)
    res.isDefined should be (true)
    val httpAccessEntry = res.get
    httpAccessEntry.ipAddress should be("123.123.123.123")
    httpAccessEntry.httpPath should be("/tutorial/spring-hibernate-grpc/")
    httpAccessEntry.httpMethod should be("GET")
    httpAccessEntry.date != null should be(true)
    httpAccessEntry.httpResponseCode should be(200)
  }
}
